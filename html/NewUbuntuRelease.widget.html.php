<?php

    // Getting all of our information
    $UpcomingUbuntu     = NewUbuntuRelease::UbuntuName();
    $UbuntuReleaseDate  = NewUbuntuRelease::UbuntuReleaseDate($UpcomingUbuntu);
    $CountDown          = NewUbuntuRelease::CountDown("$UbuntuReleaseDate");

?>


<div class="aside" id="NewUbuntuRelease" onclick="window.open('https://www.ubuntu.com/download/','_newtab');" title="Ubuntu.com: The leading OS for PC, tablet, phone and cloud">

    <div class="CountDown"><?php echo $CountDown; ?></div>

    <p id="UpcomingUbuntu"><?php echo $UpcomingUbuntu ?></p>

    <p class="ReleaseDate"><?php echo $UbuntuReleaseDate; ?></p>

</div>