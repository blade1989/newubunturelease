<?php defined('PUBLIC_HTML') OR http_response_code('404') AND die('');
/**
 *
 *  Author      :   Imri Paloja
 *  homepage    :   eurobytes.nl
 *  Description :   This will automatically display the upcoming Ubuntu version, with the Ubuntu name, date and count down, to said date, displayed 
 *
 */

class NewUbuntuRelease {

    const title = 'New Ubuntu Releases Widget';

    const description = 'Automatically display the upcoming Ubuntu version, with the Ubuntu name, date and count down, to said date, displayed';

    const author = 'Imri Paloja';

    const website = 'https://eurobytes.nl/';

    const version = '0.0.4';

    const name = 'NewUbuntuRelease';

    const update_method = 'file';

    // The direct link to the file or git repo
    const update_url = 'https://gitlab.com/blade1989/newubunturelease/-/tags';

    public function update_url() {
        # The code is hosted on gitlab. New releases are marked by tags.

        // Our update url
        $update_url = 'https://gitlab.com/blade1989/newubunturelease/-/tags';

        // Getting the content of the update url.
        $update_url_content = cURL::get("$update_url");

        // Getting the latest version number
        preg_match('/releases#[0-9\.]*/i', "$update_url_content", $effe);
        # releases#0.0.3

        // Only get the numeric values.
        preg_match('/[0-9\.]*$/i', 'releases#0.0.3', $version_number);
        # 0.0.3

        return 'https://gitlab.com/blade1989/newubunturelease/-/archive/' . $version_number['0'] . '/newubunturelease-' . $version_number['0'] . '.tar.gz';
        # https://gitlab.com/blade1989/newubunturelease/-/archive/0.0.3/newubunturelease-0.0.3.tar.gz

    }

    // The extra libraries needed for this page.
    public static $libs = array(
        'js' => array(
//            'jquery.flexslider-min.js'  => 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/jquery.flexslider-min.js'
        ),
        'css' => array(
//            'flexslider.min.css'        => 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/flexslider.min.css'
        ),
    );

   public static function UbuntuName() {

       $fp = trim(file_get_contents('http://cdimage.ubuntu.com/daily-live/current/HEADER.html'));
       if (!$fp)
           return null;

       $res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
       # Ubuntu 20.04 LTS (Focal Fossa) Daily Build

//       if (!$res)
//           return null;

       // Clean up title: remove EOL's and excessive whitespace.
       $title = preg_replace('/Daily Build/i', '', preg_replace('/\s+/', ' ', $title_matches[1]));
       #   Ubuntu 20.04 LTS (Focal Fossa)
       $title = trim($title);
       # Ubuntu 20.04 LTS (Focal Fossa)

       return $title;
       # Ubuntu 20.04 LTS (Focal Fossa)

    }

    public static function UbuntuReleaseDate($UpcomingUbuntu) {
        # Ubuntu 20.04 LTS (Focal Fossa)
        #
        // Catching only the name of the upcoming Ubuntu
        preg_match("/\(*[a-zA-Z0-9 ]*\)/i","$UpcomingUbuntu", $UbuntuName);
        # (Focal Fossa)

        $UbuntuNamePerfect = preg_replace('/[\(\) ]/', '', $UbuntuName['0']);
        # FocalFossa

        // Getting our final release date
        if (preg_match("/ *[a-zA-Z0-9 \n\t]* FinalRelease/i", strip_tags(file_get_contents("https://wiki.ubuntu.com/$UbuntuNamePerfect/ReleaseSchedule")), $blob)) {
            #  LanguagePackTranslationDeadline
            #
            #            27
            #   April 23rd
            #    FinalRelease

            // Removing al non alphanumeric charachters
            $Almost = preg_replace('/[\n\r]/', '', $blob['0']);
            #   LanguagePackTranslationDeadline    27    April 23rd      FinalRelease;

            return trim(preg_replace('/LanguagePackTranslationDeadline [0-9 ]*/', '', $Almost));
            # April 23rd      FinalRelease

        }
    }

    public static function CountDown($RoughDate) {
        # April 23rd      FinalRelease

        $Months = array("January" => '01',"February" => '02',"March" => '03',"April" => '04',"May" => '05',"June" => '06',"Juli" => '07',"August" => '08',"September" => '09',"October" => '10',"November" => '11',"December" => '12');

        $YearNumber = date('Y');
        # 2020

        // Catching the rough date and putting it in an array
        preg_match_all("/[a-zA-Z0-9]* /i", trim($RoughDate), $Date);
        #        array(1) {
        #            [0]=>
        #  array(7) {
        #                [0]=>
        #    string(6) "April "
        #                [1]=>
        #    string(5) "23rd "
        #                [2]=>
        #    string(1) " "
        #                [3]=>
        #    string(1) " "
        #                [4]=>
        #    string(1) " "
        #                [5]=>
        #    string(1) " "
        #                [6]=>
        #    string(1) " "
        #  }
        #}

        $MonthName = trim($Date['0']['0']);
        # April

        $Month = $Months["$MonthName"];
        # 04

        $DayOfMonth = trim($Date['0']['1']);
        #  23rd

        preg_match("/[0-9]*/i", $DayOfMonth, $DayNumber);
        $Day = $DayNumber['0'];
        # 23

        return "<script>countdown('$YearNumber', '$Month', '$Day')</script>";

    }

    // Sadly it seems that the, Soon to be released Ubuntu version, does not have its Mascot image hosted.
    // This only seems to be hosted by Ubuntu when it's already released. Which, by that time, is no longer needed,
    // because a new Ubuntu version is already in the making.
    public static function UbuntuImage() {
        /*
     Try to automate this with php:
     curl -s 'https://ubuntu.com/download' | grep 'https://res.cloudinary.com/canonical/image/fetch/f_auto,q_auto,fl_sanitize,w_250,h_250'
     src="https://res.cloudinary.com/canonical/image/fetch/f_auto,q_auto,fl_sanitize,w_250,h_250/https://assets.ubuntu.com/v1/f11985a2-Eoan+Ermine-gradient-outline.svg"

 */
        $UbuntuImageUrl = preg_replace('/\//', '\/', 'https://res.cloudinary.com/canonical/image/fetch/f_auto,q_auto,fl_sanitize,w_250,h_250');

        preg_match("/$UbuntuImageUrl*[a-zA-Z0-9\.\/\-\+\:]*/i", file_get_contents('https://ubuntu.com/download'), $image);
        return $image['0'];

    }

}

$NewUbuntuRelease = new NewUbuntuRelease();